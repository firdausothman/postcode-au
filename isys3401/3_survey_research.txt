Survey is used to explore rather than explain

Gathers data from respondents that are representative of some population using an instrument composed of closed stucture and open-ended items (questions)

Data Collected:
Behaviour, frequency of checking emails
Attitude/beliefs/opions, do you think workload is high
Characteristics, how tall are you
Expectations, do you think you will get higher pay
Self-classificiation, do you think you are awesome?
Knowledge

Surveys may collect facts and opinions

Internal Validity
	Are we asking the right questions
	Do the answers reflect respondents' opinions?
	Questionnaire design is important.

External Validity
	Can the findings be generalised?
	Is sample large and representative?
	Sampling technique is important.

Surverys have large number of respondents, usually measures more variables

Coss-sectional design: Collects data at one time
Longitudinal Design: takes place over time with two or more data collections
Trend design: same questions, different samples
Panel studies: different times, same respondents

Sampling - seek knowledge about population. Observe several members (sample). Extend our findings to entire class.

Sampling saves time and money, heterogenity

Probability/representative sampling: random chance of any member being picked, can estimate characteristics of population from sampling.
4 stages:
	identify sampling frame (list from which sample is selected)
	determine sample size 
	select sampling procedure
	check that sample is representative of population

Judgemental Sampling: probability of each case being selected is not known, cannot make statistical inferences about characteristics of the population

Sample size is determined by confidence required, margin of error you can tolerate, size of total population.
Most work with 95% level of certainty.

Total response rate = total number of responses / ( sample size - ineligible)
Active response rate = total number of responses / (sample size - ineligible - unreachable)

Face-to-face interviewing high cost, high response
telephone 45-60% of cost of f2f, high response rate
self-administered, hand delivered, mail survey, web survey

Standard error = sqrt(p(1-p)/n)
1.96 standard errors is a 95% confidence interval

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bookshelf; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE bookshelf (
    bookid serial NOT NULL,
    title character varying(300) NOT NULL,
    author character varying(300),
    isbn character varying(13),
    description character varying(1000),
    "comment" character varying(1000),
    rating integer,
    price numeric(10,2) NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    CONSTRAINT bookshelf_isbn_check CHECK (((length((isbn)::text) >= 10) AND (length((isbn)::text) <= 13))),
    CONSTRAINT bookshelf_rating_check CHECK ((((((rating = 1) OR (rating = 2)) OR (rating = 3)) OR (rating = 4)) OR (rating = 5))),
    CONSTRAINT minprice CHECK ((price >= (0)::numeric)),
    CONSTRAINT minquantity CHECK ((quantity >= 0))
);


ALTER TABLE public.bookshelf OWNER TO ivan;

--
-- Name: bookshelf_bookid_seq; Type: SEQUENCE SET; Schema: public; Owner: ivan
--

SELECT pg_catalog.setval(pg_catalog.pg_get_serial_sequence('bookshelf', 'bookid'), 110, true);


--
-- Data for Name: bookshelf; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY bookshelf (bookid, title, author, isbn, description, "comment", rating, price, quantity) FROM stdin;
105	Steak Lover&#39;s Cookbook	William Rice	0761100806	Rump. Loin. Skirt. Hoof. Chuck. Flank. Butt. Sometimes it&#39;s hard to tell whether food journalist Rice gets greater pleasure from writing these meaty monosyllables or from eating the cuts of beef they name. He&#39;s a modified beef purist, which means he accepts the proposition that it&#39;s permissible to apply more than fire to a good cut of meat.	From Baked Steak to Pot Roast, learn how to cook beef in ways /everyone/ will love. This book does not &#39;talk down&#39; to you like other &#39;cookbooks,&#39; but lays out its recipes in a straightforward &#39;no nonsense&#39; fashion.	4	11.16	10
109	iii	joi	8282828282	jjj	ff\r\njjj	3	88.00	88
104	The Ice Dragon (Hardcover) 	George R. R. Martin 	0765316315	The ice dragon was a creature of legend and fear, for no man had ever tamed one. When it flew overhead, it left in its wake desolate cold and frozen land. But Adara was not afraid. For Adara was a winter child, born during the worst freeze that anyone, even the Old Ones, could remember.	This book is exactly what Martin said it would be from when he announced that it was being published separately: a children&#39;s book. 	3	10.36	189
103	Secrets of the Alchemist Dar (A Treasure&#39;s Trove)	Michael Stadther	0976061880	Michael Stadther&#39;s Secrets of the Alchemist Dar, the sequel to the New York Times bestselling A Treasure&#39;s Trove, is a fantasy story about Dark and Good Fairies, spells and eclipses.	I thought that this book would be one that would be mediocre when I started it, but good gracious, was I mistaken! It was a wonderful tale down which I could not put.	4	14.95	131
107	Letter to a Christian Nation	Sam Harris	0307265773	Thousands of people have written to tell me that I am wrong not to believe in God. 	It&#39;s a shame that not everyone in this country will read Sam Harris&#39; marvelous little book Letter to a Christian Nation. They won&#39;t but they should.	4	10.17	0
110	uihiu	iuh	8288282828			1	88.00	79
\.


--
-- Name: bookshelf_pkey; Type: CONSTRAINT; Schema: public; Owner: ivan; Tablespace: 
--

ALTER TABLE ONLY bookshelf
    ADD CONSTRAINT bookshelf_pkey PRIMARY KEY (bookid);


--
-- Name: bookshelf; Type: ACL; Schema: public; Owner: ivan
--

REVOKE ALL ON TABLE bookshelf FROM PUBLIC;
REVOKE ALL ON TABLE bookshelf FROM ivan;
GRANT ALL ON TABLE bookshelf TO ivan;
GRANT INSERT,SELECT,UPDATE ON TABLE bookshelf TO bookstore;


--
-- PostgreSQL database dump complete
--


--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP TABLE public.resetpassword;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: resetpassword; Type: TABLE; Schema: public; Owner: ivan; Tablespace: 
--

CREATE TABLE resetpassword (
    id character varying(60) NOT NULL,
    resetcode character varying(32),
    usertype character varying(1)
);


--
-- Data for Name: resetpassword; Type: TABLE DATA; Schema: public; Owner: ivan
--

COPY resetpassword (id, resetcode, usertype) FROM stdin;
1	a34e44e8482ffd4dbea0b49f7a812dba	E
\.


--
-- PostgreSQL database dump complete
--


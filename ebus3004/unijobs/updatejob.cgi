#!/usr/bin/perl
use CGI qw/:standard/;
use CGI::Pretty;
use unijobs;

print	header,
	start_html(	-title=>"Unijobs.com.au: Update Job",
			-style=>"unijobs.css");
print	"<div class='site'>",
	pheader(),
	h1("Update Job");
	if (!param) {
		jobForm();
	}
	else {
		jobFormErrors();
	}
print	pfooter(),
	"</div>",
	end_html;
